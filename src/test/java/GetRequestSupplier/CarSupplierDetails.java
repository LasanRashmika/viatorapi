package GetRequestSupplier;
import static org.hamcrest.CoreMatchers.equalTo;
import constants.EndPoints;
import io.restassured.http.ContentType;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
public class CarSupplierDetails extends TestBase {

    private String url = EndPoints.VIATOR_ENDPOINT1;

    @Test
    public void supplierFullResponse() {
        given()
                .header(config.getProperty("Key"), config.getProperty("Value"))
                .contentType(ContentType.JSON).when().get(url).
                then().extract().response().prettyPrint();

    }

    @Test
    public void productName() {
        given()
                .header(config.getProperty("Key"), config.getProperty("Value"))
                .contentType(ContentType.JSON).when().get(url).
                then().body("data.title", equalTo("Private Departure Transfer from Cannes to Nice Airport"));

    }

    @Test
    public void destinationName() {
        given()
                .header(config.getProperty("Key"), config.getProperty("Value"))
                .contentType(ContentType.JSON).when().get(url)
                .then().body("data.country", equalTo("France"));

    }

    @Test
    public void productDuration() {
        given()
                .header(config.getProperty("Key"), config.getProperty("Value"))
                .contentType(ContentType.JSON).when().get(url).
                then().body("data.duration", equalTo("1 hour"));
    }

}