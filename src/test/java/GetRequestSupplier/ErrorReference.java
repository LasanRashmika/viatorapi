package GetRequestSupplier;
import constants.EndPoints;
import io.restassured.http.ContentType;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.CoreMatchers.equalTo;

public class ErrorReference extends TestBase{

    private String url = EndPoints.VIATOR_ENDPOINT2;

    @Test
    public void testErrorReference() {
        //String url = "http://prelive.viatorapi.viator.com/service/booking/hotels?productCode=177953P1";
        //String url = EndPoints.VIATOR_ENDPOINT1;
        given().contentType(ContentType.JSON).when().get(url).then().statusCode(200);
    }

    @Test
    public void ErrorReferenceResponse(){
        //String url = "http://prelive.viatorapi.viator.com/service/booking/hotels?productCode=177953P1";

        //String url = EndPoints.VIATOR_ENDPOINT1;
        given().contentType(ContentType.JSON).when().get(url).
                then().extract().response().prettyPrint();
    }

    @Test
    public void SuccessStatus(){
        given().contentType(ContentType.JSON).when().get(url).
                then().body("success",equalTo(false));

    }

}
