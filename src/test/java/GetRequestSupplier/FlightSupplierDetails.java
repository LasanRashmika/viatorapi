package GetRequestSupplier;

import constants.EndPoints;
import io.restassured.http.ContentType;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;

public class FlightSupplierDetails extends TestBase {

    private String url = EndPoints.VIATOR_ENDPOINT3;

    @Test
    public void supplierFullResponse() {
        given()
                .header(config.getProperty("Key"), config.getProperty("Value"))
                .contentType(ContentType.JSON).when().get(url).
                then().extract().response().prettyPrint();

    }

    @Test
    public void productName() {
        given()
                .header(config.getProperty("Key"), config.getProperty("Value"))
                .contentType(ContentType.JSON).when().get(url).
                then().body("data.title", equalTo("Air Taxi & Tour Niagara - Toronto"));

    }

    @Test
    public void destinationName() {
        given()
                .header(config.getProperty("Key"), config.getProperty("Value"))
                .contentType(ContentType.JSON).when().get(url)
                .then().body("data.country", equalTo("Canada"));
    }

    @Test
    public void productDuration() {
        given()
                .header(config.getProperty("Key"), config.getProperty("Value"))
                .contentType(ContentType.JSON).when().get(url).
                then().body("data.duration", equalTo("1 hour"));
    }

}