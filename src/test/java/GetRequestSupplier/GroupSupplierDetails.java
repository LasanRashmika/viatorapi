package GetRequestSupplier;

import constants.EndPoints;
import io.restassured.http.ContentType;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;

public class GroupSupplierDetails extends TestBase {

    private String url = EndPoints.VIATOR_ENDPOINT4;

    @Test
    public void supplierFullResponse() {
        given()
                .header(config.getProperty("Key"), config.getProperty("Value"))
                .contentType(ContentType.JSON).when().get(url).
                then().extract().response().prettyPrint();
    }

    @Test
    public void productName() {
        given()
                .header(config.getProperty("Key"), config.getProperty("Value"))
                .contentType(ContentType.JSON).when().get(url).
                then().body("data.title", equalTo("Private Guided Walking Tour of Jewish Berlin History"));
    }

    @Test
    public void destinationID() {
        given()
                .header(config.getProperty("Key"), config.getProperty("Value"))
                .contentType(ContentType.JSON).when().get(url)
                .then().body("data.country", equalTo("Germany"));
    }

    @Test
    public void productDuration() {
        given()
                .header(config.getProperty("Key"), config.getProperty("Value"))
                .contentType(ContentType.JSON).when().get(url).
                then().body("data.duration", equalTo("3 hours"));
    }
}