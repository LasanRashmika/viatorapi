package GetRequestSupplier;

import constants.EndPoints;
import io.restassured.http.ContentType;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;

public class PersonSupplierDetails extends TestBase {

    private String url = EndPoints.VIATOR_ENDPOINT6;

    @Test
    public void supplierFullResponse() {
        given()
                .header(config.getProperty("Key"), config.getProperty("Value"))
                .contentType(ContentType.JSON).when().get(url).
                then().extract().response().prettyPrint();
    }

    @Test
    public void productName() {
        given()
                .header(config.getProperty("Key"), config.getProperty("Value"))
                .contentType(ContentType.JSON).when().get(url).
                then().body("data.title", equalTo("Big Bus Sydney and Bondi Hop-on Hop-off Tour"));
    }

    @Test
    public void destinationName() {
        given()
                .header(config.getProperty("Key"), config.getProperty("Value"))
                .contentType(ContentType.JSON).when().get(url)
                .then().body("data.country", equalTo("Australia"));
    }

    @Test
    public void productDuration() {
        given()
                .header(config.getProperty("Key"), config.getProperty("Value"))
                .contentType(ContentType.JSON).when().get(url).
                then().body("data.duration", equalTo("2 hours"));
    }
}