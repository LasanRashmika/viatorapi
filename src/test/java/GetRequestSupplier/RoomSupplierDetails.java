package GetRequestSupplier;

import constants.EndPoints;
import io.restassured.http.ContentType;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;

public class RoomSupplierDetails extends TestBase {

    private String url = EndPoints.VIATOR_ENDPOINT5;

    @Test
    public void supplierFullResponse() {
        given()
                .header(config.getProperty("Key"), config.getProperty("Value"))
                .contentType(ContentType.JSON).when().get(url).
                then().extract().response().prettyPrint();
    }

    @Test
    public void productName() {
        given()
                .header(config.getProperty("Key"), config.getProperty("Value"))
                .contentType(ContentType.JSON).when().get(url).
                then().body("data.title", equalTo("Taipei Day Tour including Taipei 101, Din Tai Fung and Hot Spring Experience"));
    }

    @Test
    public void destinationName() {
        given().log().all()
                .header(config.getProperty("Key"), config.getProperty("Value"))
                .contentType(ContentType.JSON).when().get(url)
                .then().body("data.country", equalTo("Taiwan"));
    }

    @Test
    public void productDuration() {
        given()
                .header(config.getProperty("Key"), config.getProperty("Value"))
                .contentType(ContentType.JSON).when().get(url).
                then().body("data.duration", equalTo("7 hours"));
    }
}