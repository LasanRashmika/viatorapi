package GetRequestSupplier;
import io.restassured.RestAssured;
import org.testng.annotations.BeforeClass;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class TestBase {
    public static Properties config;

    @BeforeClass
    public void setup() throws IOException {

        TestBase.setBaseURI("https://prelive.viatorapi.viator.com/service/");

        config = new Properties();
        FileInputStream fis = new FileInputStream("E:\\LearningMe\\apiviator\\src\\test\\java\\constants\\config.properties");
        config.load(fis);
    }

    public static void setBaseURI(String baseURI)
    {
        RestAssured.baseURI=baseURI;

    }
}