package PostRequestProduct;
import GetRequestSupplier.TestBase;
import constants.EndPoints;
import io.restassured.http.ContentType;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.CoreMatchers.equalTo;

public class PerPersonProduct extends TestBase {

    private String url = EndPoints.VIATOR_ENDPOINT7;
    public String requestBody = "{\n" +
            "  \"productCode\": \"107107P10\",\n" +
            "  \"month\": \"11\",\n" +
            "  \"year\": \"2020\",\n" +
            "  \"currencyCode\": \"GBP\"\n" +
            "}";


    @Test
    public void availability() {
        given().log().all().
                header(config.getProperty("Key"), config.getProperty("Value")).
                header("Accept", "application/json").
                header("Content-Type", "application/json").
                header("Accept-Language", "en").
                body(requestBody).
                contentType(ContentType.JSON).when().post(url).
                then().extract().response().prettyPrint();
    }

    @Test
    public void pricingUnit() {
        given().log().all().
                header(config.getProperty("Key"), config.getProperty("Value")).
                header("Accept", "application/json").
                header("Content-Type", "application/json").
                header("Accept-Language", "en").
                body(requestBody).
                contentType(ContentType.JSON).when().post(url).
                then().body("data.pricingUnit", equalTo("per person"));
    }

    @Test
    public void bookingMonth() {
        given().log().all().
                header(config.getProperty("Key"), config.getProperty("Value")).
                header("Accept", "application/json").
                header("Content-Type", "application/json").
                header("Accept-Language", "en").
                body(requestBody).
                contentType(ContentType.JSON).when().post(url).
                then().body("data.bookingMonth", equalTo("2020-11"));
    }
}