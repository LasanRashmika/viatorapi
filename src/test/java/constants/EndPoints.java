package constants;

public class EndPoints {

    public static final String VIATOR_ENDPOINT1 ="product?currencyCode=GBP&sortOrder=REVIEW_RATING_A&code=10175P10&showUnavailable=false&excludeTourGradeAvailability=true";
    public static final String VIATOR_ENDPOINT2 ="booking/hotels?productCode=177953P1";
    public static final String VIATOR_ENDPOINT3 ="product?currencyCode=GBP&sortOrder=REVIEW_RATING_A&code=14876P5&showUnavailable=false&excludeTourGradeAvailability=true";
    public static final String VIATOR_ENDPOINT4 ="product?currencyCode=GBP&sortOrder=REVIEW_RATING_A&code=10847P42&showUnavailable=false&excludeTourGradeAvailability=true";
    public static final String VIATOR_ENDPOINT5 ="product?currencyCode=GBP&sortOrder=REVIEW_RATING_A&code=6279P26&showUnavailable=false&excludeTourGradeAvailability=true";
    public static final String VIATOR_ENDPOINT6 ="product?currencyCode=GBP&sortOrder=REVIEW_RATING_A&code=5010SYDNEY&showUnavailable=false&excludeTourGradeAvailability=true";
    public static final String VIATOR_ENDPOINT7 ="booking/availability/tourgrades/pricingmatrix";





}
